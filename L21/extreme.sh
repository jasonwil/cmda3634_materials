#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 10
#SBATCH -o extreme.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the module
module load matplotlib >& /dev/null

# compile extreme
gcc -O3 -o extreme extreme.c vec.c -lm

# run on the specified input file
cat $1 | ./extreme

