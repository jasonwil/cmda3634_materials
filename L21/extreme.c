#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vec.h"

typedef struct {
    double max_dist_sq;
    int extreme_1, extreme_2;
} extreme_info;

extreme_info extreme_pair (double* data, int len, int dim) {
    extreme_info info = { 0, -1, -1 };
    for (int i=0;i<len-1;i++) {
        for (int j=i+1;j<len;j++) {
            // data+i*dim is a pointer to the ith data point
            // data+j*dim is a pointer to the jth data point
            double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
            if (dist_sq > info.max_dist_sq) {
                info.max_dist_sq = dist_sq;
                info.extreme_1 = i;
                info.extreme_2 = j;
            }
        }
    }
    return info;
}

int main () {

    // read dataset with len points in R^dim
    int len, dim;
    if (scanf("%d %d",&len,&dim) != 2) {
        printf ("error reading the length and dimension of the dataset\n");
        return 1;
    }
    // allocate the (len x dim) data matrix on the heap using malloc
    double* data = (double*)malloc(len*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc failed to allocate data matrix\n");
        return 1;
    }
    vec_read_dataset (data,len,dim);

    // find the extreme pair
    extreme_info info;
    info = extreme_pair(data,len,dim);

    // print the results
    printf ("Extreme Distance = %.2f\n",sqrt(info.max_dist_sq));
    printf ("Extreme Pair = %d %d\n",info.extreme_1,info.extreme_2);

    // free the data matrix
    free (data);
}
