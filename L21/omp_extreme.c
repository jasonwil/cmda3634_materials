#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "vec.h"

typedef struct {
    double max_dist_sq;
    int extreme_1, extreme_2;
} extreme_info;

extreme_info extreme_pair (double* data, int len, int dim) {
    extreme_info info = { 0, -1, -1 };
#pragma omp parallel default(none) shared(len,dim,data,info)
    {
        extreme_info thread_info;
#pragma omp for collapse(2)
        for (int i=0;i<len-1;i++) {
            for (int j=i+1;j<len;j++) {
                double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
                if (dist_sq > thread_info.max_dist_sq) {
                    thread_info.max_dist_sq = dist_sq;
                    thread_info.extreme_1 = i;
                    thread_info.extreme_2 = j;
                }
            }
        }
#pragma omp critical
        {
            if (thread_info.max_dist_sq > info.max_dist_sq) {
                info = thread_info;
            }
        }
    }
    return info;
}

int main (int argc, char** argv) {

    // get num_threads from the command line
    if (argc < 2) {
        printf ("Command usage : %s num_threads\n",argv[0]);
        return 1;
    }
    int num_threads = atoi(argv[1]);
    omp_set_num_threads(num_threads);

    // read dataset with len points in R^dim
    int len, dim;
    if (scanf("%d %d",&len,&dim) != 2) {
        printf ("error reading the length and dimension of the dataset\n");
        return 1;
    }
    // allocate the (len x dim) data matrix on the heap using malloc
    double* data = (double*)malloc(len*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc failed to allocate data matrix\n");
        return 1;
    }
    vec_read_dataset (data,len,dim);

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();

    // find the extreme pair
    extreme_info info;
    info = extreme_pair(data,len,dim);

    // stop the timer
    end_time = omp_get_wtime();

    // print the results
#ifdef TIMING
    printf ("(%d,%.4f),",num_threads,end_time-start_time);
#else
    printf ("num_threads = %d, ",num_threads);
    printf ("elapsed time = %.4f seconds\n",end_time-start_time);
    printf ("Extreme Distance = %.2f\n",sqrt(info.max_dist_sq));
    printf ("Extreme Pair = %d %d\n",info.extreme_1,info.extreme_2);
#endif

    // free the data matrix
    free (data);
}
