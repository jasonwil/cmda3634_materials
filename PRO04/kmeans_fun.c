#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "vec.h"
#include "kmeans_fun.h"

// calculate the arg max for farthest first
int calc_arg_max (double* data, int len, int dim, int* centers, int m) {
    int arg_max;
    double cost_sq = 0;
    for (int i=0;i<len;i++) {
        double min_dist_sq = DBL_MAX;
        for (int j=0;j<m;j++) {
            double dist_sq = vec_dist_sq(data+i*dim,data+centers[j]*dim,dim);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
            arg_max = i;
        }
    }
    return arg_max;
}

// for every point find the cluster index (i.e. the index of the closest mean)
// clusters is an output array of size len that contains the cluster index of each point
void find_clusters (double* data, int len, int dim, double* kmeans, int k, int* clusters) {

    /**********************/
    /* Add Your Code Here */
    /**********************/
    
}

// calculate the next kmeans
// clusters is an input array of size len that contains the cluster index of each point
// kmeans_next is an output array of size k*dim that contains the next kmeans
// do not assume that the kmeans_next array has been initialized
void calc_kmeans_next (double* data, int len, int dim, double* kmeans_next, int k, int* clusters) {

    /**********************/
    /* Add Your Code Here */
    /**********************/
    
}
