#ifndef VEC_H
#define VEC_H

// calculate the distance squared between dim dimensional vectors u and v
double vec_dist_sq (double* u, double* v, int dim);

// read len vectors in dim dimensional space from stdin into data array
void vec_read_dataset (double* data, int len, int dim);

// v = 0
void vec_zero (double* v, int dim);

// w = u + v
void vec_add (double* u, double* v, double* w, int dim);

// w = cv
void vec_scalar_mult (double* v, double c, double* w, int dim);

// performs the copy v->data[i] = w->data[i] for all i
void vec_copy (double* v, double* w, int dim);

#endif
