#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "vec.h"
#include "kmeans_fun.h"

int main (int argc, char** argv) {

    // get k and num_iter from command line
    if (argc < 3) {
        printf ("Command usage : %s k num_iter\n",argv[0]);
        return 1;
    }
    int k = atoi(argv[1]);
    int num_iter = atoi(argv[2]);

    // read the number of points and the dimension of each point    
    int len, dim;
    if (scanf("%d %d",&len,&dim) != 2) {
        printf ("error reading the length and dimension of the dataset\n");
        return 1;
    }

    // allocate the data array on the heap using malloc
    double* data = (double*)malloc(len*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc failed to allocate data array\n");
        return 1;
    }

    // read the dataset from stdin
    vec_read_dataset (data,len,dim);

    // allocate the clusters array
    int* clusters = (int*)malloc(len*sizeof(int));
    
    // find k centers using the farthest first algorithm
    int centers[k];
    centers[0] = 0;
    for (int m=1;m<k;m++) {
        centers[m] = calc_arg_max(data,len,dim,centers,m);
    }

    // initialize kmeans using the k centers from farthest first
    double kmeans[k*dim];
    for (int i=0;i<k;i++) {
        vec_copy(kmeans+i*dim,data+centers[i]*dim,dim);
    }

    // update kmeans m times
    double kmeans_next[k*dim];
    for (int i=0;i<num_iter;i++) {
	find_clusters(data,len,dim,kmeans,k,clusters);
        calc_kmeans_next(data,len,dim,kmeans_next,k,clusters);
        vec_copy(kmeans,kmeans_next,k*dim);
    }

    // print the results
    for (int i=0;i<k;i++) {
        for (int j=0;j<dim;j++) {
            printf ("%.5f ",kmeans[i*dim+j]);
        }
        printf ("\n");
    }
    
    // free the dynamically allocated memory
    free (data);
    free (clusters);
}
