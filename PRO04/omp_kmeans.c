#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <omp.h>
#include "vec.h"
#include "kmeans_fun.h"

int main (int argc, char** argv) {

    // get k, m, and num_threads from command line
    if (argc < 4) {
        printf ("Command usage : %s k m num_threads\n",argv[0]);
        return 1;
    }
    int k = atoi(argv[1]);
    int m = atoi(argv[2]);
    int num_threads = atoi(argv[3]);
    omp_set_num_threads(num_threads);

    // read the number of points and the dimension of each point    
    int len, dim;
    if (scanf("%d %d",&len,&dim) != 2) {
        printf ("error reading the length and dimension of the dataset\n");
        return 1;
    }

    // allocate the data array on the heap using malloc
    double* data = (double*)malloc(len*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc failed to allocate data array\n");
        return 1;
    }

    // read the dataset from stdin
    vec_read_dataset (data,len,dim);

    // allocate the clusters array
    int* clusters = (int*)malloc(len*sizeof(int));

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();
    
    // find k centers using the farthest first algorithm
    int centers[k];
    centers[0] = 0;
    for (int m=1;m<k;m++) {
        centers[m] = calc_arg_max(data,len,dim,centers,m);
    }

    // initialize kmeans using the k centers from farthest first
    double kmeans[k*dim];
    for (int i=0;i<k;i++) {
        vec_copy(kmeans+i*dim,data+centers[i]*dim,dim);
    }

    // update kmeans m times
    double kmeans_next[k*dim];
    for (int i=0;i<m;i++) {
	find_clusters(data,len,dim,kmeans,k,clusters);
        calc_kmeans_next(data,len,dim,kmeans_next,k,clusters);
        vec_copy(kmeans,kmeans_next,k*dim);
    }

    // stop the timer
    end_time = omp_get_wtime();

#ifdef TIMING
    printf ("(%d,%.4f),",num_threads,(end_time-start_time));
#else
    // print out the number of threads
    printf ("# num_threads = %d\n",num_threads);
    
    // print out wall time used
    printf ("# wall time used = %.4f sec\n",end_time-start_time);

    // print the results
    for (int i=0;i<k;i++) {
        for (int j=0;j<dim;j++) {
            printf ("%.5lf ",kmeans[i*dim+j]);
        }
        printf ("\n");
    }
#endif
    
    // free the dynamically allocated memory
    free (data);
    free (clusters);
}
