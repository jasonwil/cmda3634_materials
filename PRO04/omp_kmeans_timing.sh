# Build the executable
gcc -DTIMING -o omp_kmeans omp_kmeans.c omp_kmeans_fun.c vec.c -fopenmp

# run omp_kmeans
cat $1 | ./omp_kmeans $2 $3 1
cat $1 | ./omp_kmeans $2 $3 2
cat $1 | ./omp_kmeans $2 $3 4
cat $1 | ./omp_kmeans $2 $3 8
cat $1 | ./omp_kmeans $2 $3 16
cat $1 | ./omp_kmeans $2 $3 32

# print newline
echo
