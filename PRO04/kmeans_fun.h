#ifndef KMEANS_FUN_H
#define KMEANS_FUN_H

// calculate the arg max for farthest first
int calc_arg_max (double* data, int len, int dim, int* centers, int m);

// for every point find the cluster index (i.e. the index of the closest mean)
// clusters is an output array of size len that contains the cluster index of each point
void find_clusters (double* data, int len, int dim, double* kmeans, int k, int* clusters);

// calculate the next kmeans
// clusters is an input array of size len that contains the cluster index of each point
// kmeans_next is an output array of size k*dim that contains the next kmeans
// do not assume that the kmeans_next array has been initialized
void calc_kmeans_next (double* data, int len, int dim, double* kmeans_next, int k, int* clusters);

#endif
