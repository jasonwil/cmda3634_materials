# Build the executable
gcc -o omp_kmeans omp_kmeans.c omp_kmeans_fun.c vec.c -fopenmp

# run omp_kmeans
cat $1 | ./omp_kmeans $2 $3 4
