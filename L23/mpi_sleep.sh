#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_sleep.out

# check to make sure we provided a command line argument for the seconds to sleep
if [ "$#" -ne 1 ]
then
  echo "error: please specify the seconds to sleep as a command line argument"
  exit 1
fi

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_sleep mpi_sleep.c

# run mpi_sleep on $SLURM_NTASKS ranks
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sleep $1
