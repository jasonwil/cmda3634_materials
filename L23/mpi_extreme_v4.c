#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "vec.h"

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // read the filename from the command line
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"filename");
        return 1;
    }
    char* filename = argv[1];

    // open the text file for reading
    FILE* fptr;
    fptr = fopen(filename,"r");

    // need to check for null
    if (fptr == 0) {
        printf ("Error opening data file %s.\n",filename);
        exit(1);
    }

    // read the number of points and the dimension of each point
    int len, dim;
    if (fscanf(fptr,"%d %d",&len, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // allocate the (len x dim) data matrix on the heap using malloc
    double* data = (double*)malloc(len*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc failed to allocate data matrix\n");
        return 1;
    }
    vec_read_dataset_file (fptr,data,len,dim);

    // close the data file
    fclose(fptr);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme[2];
#ifdef DIAG
    int pairs_checked = 0;
#endif
    for (int i=0+rank;i<len-1;i+=size) {
        for (int j=i+1;j<len;j++) {
            double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
#ifdef DIAG
            pairs_checked += 1;
#endif
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme[0] = i;
                extreme[1] = j;
            }
        }
    }

    // all nonzero ranks send their extreme pair to rank 0
    if (rank == 0) {
        MPI_Status status;
        int rank_extreme[2];
        for (int source=1;source<size;source++) {
            MPI_Recv(rank_extreme,2,MPI_INT,source,0,MPI_COMM_WORLD,&status);
            int i = rank_extreme[0];
            int j = rank_extreme[1];
            double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme[0] = i;
                extreme[1] = j;
            }
        }
    } else {
        MPI_Send(extreme,2,MPI_INT,0,0,MPI_COMM_WORLD);
    }


    // stop the timer
    end_time = MPI_Wtime();

    // output the results
#ifdef DIAG
    printf ("rank %d: pairs checked = %d\n",rank,pairs_checked);
#endif
    if (rank == 0) {
	printf ("rank %d: elapsed time = %.4f seconds\n",rank,end_time-start_time);
	printf ("rank %d: Extreme Distance = %.2f\n",rank,sqrt(max_dist_sq));
	printf ("rank %d: Extreme Pair = %d %d\n",rank,extreme[0],extreme[1]);
    }

    // free memory allocated for dataset
    free(data);

    MPI_Finalize();
}
