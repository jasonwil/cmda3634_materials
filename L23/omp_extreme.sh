#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 10
#SBATCH --cpus-per-task=4
#SBATCH -o omp_extreme.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the module
module load matplotlib >& /dev/null

# compile ompo_extreme
gcc -o omp_extreme omp_extreme.c vec.c -lm -fopenmp

# run on the specified input file
cat $1 | ./omp_extreme 4

