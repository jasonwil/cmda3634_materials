#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH -o mpi_extreme_v4.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_extreme_v4 mpi_extreme_v4.c vec.c -lm

# run mpi_sleep on $SLURM_NTASKS ranks
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_extreme_v4 $1
