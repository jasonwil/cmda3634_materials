#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h> // for the usleep function

int main(int argc, char** argv) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // get number of seconds to sleep from command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"time");
        return 1;
    }
    int time = atoi(argv[1]);

    printf ("MPI rank %d of %d running on %s taking a %d second nap!\n",
	    rank,size,node_name,time);    
    usleep (1000000*time); // wait time seconds

    MPI_Finalize();
}

