import sys
import numpy as np
import time

# load the points
data = np.loadtxt(sys.stdin,skiprows=1)

# calculate the cost squared for centers with indices c1, c2
def calc_cost_sq(data,c1,c2):
    cost_sq = 0;
    num_points = len(data)
    for i in range(num_points):
        ds1 = np.inner(data[i]-data[c1],data[i]-data[c1])
        ds2 = np.inner(data[i]-data[c2],data[i]-data[c2])
        min_dist_sq = min([ds1,ds2])
        cost_sq = max([cost_sq,min_dist_sq])
    return cost_sq

# start the timer
tic = time.process_time()

# compute the minimal cost and an optimal solution
num_points = len(data)
min_cost_sq = float("inf")
tuples_checked = 0;
for i in range(0,num_points-1):
    for j in range(i+1,num_points):
        tuples_checked += 1
        cost_sq = calc_cost_sq(data,i,j)
        if (cost_sq < min_cost_sq):
            min_cost_sq = cost_sq
            optimal_centers = np.array([i,j])
            
# stop the timer
toc = time.process_time()
elapsed = toc-tic

# print the results
print ('number of points =',num_points)
print ('2-tuples checked =',tuples_checked)
print ('elapsed time =',round(elapsed,2),'seconds')
print ('2-tuples checked per second =',int(tuples_checked/elapsed))
print ('minimum cost =',np.round(np.sqrt(min_cost_sq),2))
print ('optimal centers =',optimal_centers)
