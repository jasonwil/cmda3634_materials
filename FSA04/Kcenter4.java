import java.util.Scanner;

// class for a 2d vector
class Vec2 {
    double x,y;

    public Vec2 (double x, double y) {
        this.x = x;
        this.y = y;
    }
};

class Kcenter4 {

    static double dist_sq (Vec2 u, Vec2 v) {
        return (u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y);
    }

    // calc the cost squared for centers with indices c1, c2, c3, c4
    static double calc_cost_sq (Vec2 data[], int c1, int c2, int c3, int c4) {
        int num_points = data.length;
        double cost_sq = 0;
        for (int i=0;i<num_points;i++) {	
            double ds1 = dist_sq(data[i],data[c1]);
            double ds2 = dist_sq(data[i],data[c2]);
            double ds3 = dist_sq(data[i],data[c3]);
            double ds4 = dist_sq(data[i],data[c4]);	    
            double min_dist_sq = Math.min(Math.min(ds1,ds2),Math.min(ds3,ds4));
            cost_sq = Math.max(min_dist_sq,cost_sq);
        }
        return cost_sq;
    }

    public static void main(String args[]) {

        // create a scanner for standard input
        Scanner input = new Scanner(System.in);

        // read the number of points
        int num_points = input.nextInt();

        // read the data matrix
        Vec2 data[] = new Vec2[num_points];
        for (int i=0;i<num_points;i++) {
            double x = input.nextDouble();
            double y = input.nextDouble();
            data[i] = new Vec2(x,y);
        }

        // start the timer
        long start = System.nanoTime();

        // compute the minimal cost and an optimal solution
        double min_cost_sq = Double.MAX_VALUE;
        int optimal_centers[] = new int[4];
        int tuples_checked = 0;
        for (int i=0; i<num_points-3; i++) {
            for (int j=i+1; j<num_points-2; j++) {
                for (int k=j+1; k<num_points-1; k++) {
                    for (int l=k+1; l<num_points; l++) {		    
                        tuples_checked += 1;
                        double cost_sq = calc_cost_sq (data,i,j,k,l);
                        if (cost_sq < min_cost_sq) {
                            min_cost_sq = cost_sq;
                            optimal_centers[0] = i;
                            optimal_centers[1] = j;
                            optimal_centers[2] = k;
                            optimal_centers[3] = l;
                        }
                    }
                }
            }
        }

        // stop the timer
        long stop = System.nanoTime();
        double elapsed = (double)(stop-start)/1.0e9;

        // print the results
        System.out.printf ("number of points = %d\n",num_points);
        System.out.printf ("4-tuples checked = %d\n",tuples_checked);
        System.out.printf ("elapsed time = %.2f seconds\n",elapsed);
        System.out.printf ("4-tuples checked per second = %.0f\n",tuples_checked/elapsed);
        System.out.printf ("minimal cost = %.2f\n",Math.sqrt(min_cost_sq));
        System.out.printf ("optimal centers : %d %d %d %d\n",optimal_centers[0],
                optimal_centers[1],optimal_centers[2],optimal_centers[3]);
    }
};
