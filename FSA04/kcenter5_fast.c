#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h>

// structure for a 2d vector
typedef struct vec2_s {
    double x,y;
} vec2_type;

// functions for a 2d vector
double vec2_dist_sq (vec2_type u, vec2_type v) {
    return (u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y);
}

// calc the cost squared for centers with indices c1, c2, c3, c4, c5
double calc_cost_sq (double* dist_sqs, int num_points, int c1, int c2, int c3, int c4, int c5,
        double min_cost_sq) {
    double cost_sq = 0;
    for (int i=0;i<num_points;i++) {
        double ds1 = dist_sqs[i*num_points+c1];
        double ds2 = dist_sqs[i*num_points+c2];
        double ds3 = dist_sqs[i*num_points+c3];
        double ds4 = dist_sqs[i*num_points+c4];
        double ds5 = dist_sqs[i*num_points+c5];
        double min_dist_sq = fmin(fmin(ds1,ds2),fmin(fmin(ds3,ds4),ds5));
        cost_sq = fmax(min_dist_sq,cost_sq);
        // check to see if we can abort the cost calculation early
        if (cost_sq > min_cost_sq) {
            break;
        }
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    // read the number of points
    int num_points;
    if (scanf("%d",&num_points) != 1) {
        printf ("error reading the number of points\n");
        return 1;
    }

    // read the data matrix
    vec2_type data[num_points];
    for (int i=0;i<num_points;i++) {
        if (scanf("%lf %lf",&(data[i].x),&(data[i].y)) != 2) {
            printf ("error reading the data matrix\n");
            return 1;
        }
    }

    // start the timer
    clock_t start = clock();

    // calculate the distances squared table
    double dist_sqs[num_points*num_points];
    for (int i=0;i<num_points;i++) {
        for (int j=0;j<num_points;j++) {
            dist_sqs[i*num_points+j] = vec2_dist_sq (data[i],data[j]);
        }
    }

    // compute the minimal cost and an optimal solution
    double min_cost_sq = DBL_MAX;
    int optimal_centers[5] = { 0 }; // C does not automatically initialize arrays!
    int tuples_checked = 0;
    for (int i=0; i<num_points-4; i++) {
        for (int j=i+1; j<num_points-3; j++) {
            for (int k=j+1; k<num_points-2; k++) {
                for (int l=k+1; l<num_points-1; l++) {
                    for (int m=l+1; m<num_points; m++) {
                        tuples_checked += 1;
                        double cost_sq = calc_cost_sq(dist_sqs,num_points,i,j,k,l,m,min_cost_sq);
                        if (cost_sq < min_cost_sq) {
                            min_cost_sq = cost_sq;
                            optimal_centers[0] = i;
                            optimal_centers[1] = j;
                            optimal_centers[2] = k;
                            optimal_centers[3] = l;
                            optimal_centers[4] = m;
                        }
                    }
                }
            }
        }
    }

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;

    // print the results
    printf ("number of points = %d\n",num_points);
    printf ("5-tuples checked = %d\n",tuples_checked);
    printf ("elapsed time = %.2f seconds\n",elapsed);
    printf ("5-tuples checked per second = %.0f\n",tuples_checked/elapsed);    
    printf ("minimal cost = %.2f\n",sqrt(min_cost_sq));
    printf ("optimal centers : %d %d %d %d %d\n",optimal_centers[0],
            optimal_centers[1],optimal_centers[2],optimal_centers[3],optimal_centers[4]);
}
