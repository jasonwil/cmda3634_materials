#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h>

// structure for a 2d vector
typedef struct vec2_s {
    double x,y;
} vec2_type;

// functions for a 2d vector
double vec2_dist_sq (vec2_type u, vec2_type v) {
    return (u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y);
}

// calc the cost squared for centers with indices c1, c2, c3, c4
double calc_cost_sq (vec2_type* data, int num_points, int c1, int c2, int c3, int c4) {
    double cost_sq = 0;
    for (int i=0;i<num_points;i++) {
        double ds1 = vec2_dist_sq(data[i],data[c1]);
        double ds2 = vec2_dist_sq(data[i],data[c2]);
        double ds3 = vec2_dist_sq(data[i],data[c3]);
        double ds4 = vec2_dist_sq(data[i],data[c4]);
        double min_dist_sq = fmin(fmin(ds1,ds2),fmin(ds3,ds4));
        cost_sq = fmax(min_dist_sq,cost_sq);
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    // read the number of points
    int num_points;
    if (scanf("%d",&num_points) != 1) {
        printf ("error reading the number of points\n");
        return 1;
    }

    // read the data matrix
    vec2_type data[num_points];
    for (int i=0;i<num_points;i++) {
        if (scanf("%lf %lf",&(data[i].x),&(data[i].y)) != 2) {
            printf ("error reading the data matrix\n");
            return 1;
        }
    }

    // start the timer
    clock_t start = clock();

    // compute the minimal cost and an optimal solution
    double min_cost_sq = DBL_MAX;
    int optimal_centers[4] = { 0 }; // C does not automatically initialize arrays!
    int tuples_checked = 0;
    for (int i=0; i<num_points-3; i++) {
        for (int j=i+1; j<num_points-2; j++) {
            for (int k=j+1; k<num_points-1; k++) {
                for (int l=k+1; l<num_points; l++) {
                    tuples_checked += 1;
                    double cost_sq = calc_cost_sq(data,num_points,i,j,k,l);
                    if (cost_sq < min_cost_sq) {
                        min_cost_sq = cost_sq;
                        optimal_centers[0] = i;
                        optimal_centers[1] = j;
                        optimal_centers[2] = k;
                        optimal_centers[3] = l;
                    }
                }
            }
        }
    }

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;

    // print the results
    printf ("number of points = %d\n",num_points);
    printf ("4-tuples checked = %d\n",tuples_checked);
    printf ("elapsed time = %.2f seconds\n",elapsed);
    printf ("4-tuples checked per second = %.0f\n",tuples_checked/elapsed);    
    printf ("minimal cost = %.2f\n",sqrt(min_cost_sq));
    printf ("optimal centers : %d %d %d %d\n",optimal_centers[0],
            optimal_centers[1],optimal_centers[2],optimal_centers[3]);
}
