#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 10
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_kcenter_debug.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib/3.5.2-foss-2022a

# Build the executable
mpicc -O3 -o mpi_kcenter mpi_kcenter.c -lm

# run mpi_work on nodes*ntasks-per-node cores
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kcenter $1 $2 $3 $4

# The script will exit whether we give the "exit" command or not.
exit
