import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 2):
    print ("Command Usage : python3",sys.argv[0],"outfile")
    exit(1)
outfile = sys.argv[1]

# read the data file
data = np.loadtxt(sys.stdin,skiprows=1)

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=5,color='maroon')

# plot the special points (if additional command line argments present)
for k in range(2,len(sys.argv)):
    i = int(sys.argv[k])
    plt.scatter (data[i,0],data[i,1],s=50,color='orange')

# save the plot as an image
plt.savefig(outfile)
