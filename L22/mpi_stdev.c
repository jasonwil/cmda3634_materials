#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // compute the sum
    long long sum = 0;
    for (long long i=1;i<=N;i++) {
	sum += i;
    }

    // compute the mean
    double mean = 1.0*sum/N;

    // compute the sum of differences squared
    double sum_diff_sq = 0;
    for (long long i=1;i<=N;i++) {
	sum_diff_sq += (i-mean)*(i-mean);
    }

    // stop the timer
    end_time = MPI_Wtime();
    
    // only rank 0 computes and prints the standard deviation
    if (rank == 0) {

	// calculate the variance
	double variance = sum_diff_sq/N;

	// calculate the standard deviation
	double stdev = sqrt(variance);

        printf ("num ranks = %d, elapsed time = %.2f seconds\n",
		size,end_time-start_time);
        printf ("standard deviation is %.3lf, sqrt((N^2-1)/12) is %.3lf\n",
                stdev,sqrt((N*N-1)/12.0));
    }
    
    MPI_Finalize();
}
