#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // each rank computes a partial sum
    long long sum = 0;
    for (long long i=1+rank;i<=N;i+=size) {
        sum += i;
    }

    // use parallel message passing to reduce the partial sums with result on rank 0
    // we assume that size = 2^k for some integer k >= 0
    int alive = size;
    while (alive > 1) {
        if (rank < alive/2) {
            // rank is a receiver
            long long rank_sum;
            MPI_Status status;
            int src = rank + alive/2;
            MPI_Recv (&rank_sum, 1, MPI_LONG_LONG, src, 0, MPI_COMM_WORLD, &status);
            sum += rank_sum;
        } else if (rank < alive) {
            // rank is a sender */
            int dest = rank - alive/2;
            MPI_Send (&sum, 1, MPI_LONG_LONG, dest, 0, MPI_COMM_WORLD);
        }
        alive = alive/2;
    }

    // use parallel message passing to broadcast the sum on rank 0 to all other ranks
    // we assume that size = 2^k for some integer k >= 0
    alive = 1;
    while (alive < size) {
        alive = alive*2;
        if (rank < alive/2) {
            // rank is a sender
            int dest = rank + alive/2;
            MPI_Send (&sum, 1, MPI_LONG_LONG, dest, 0, MPI_COMM_WORLD);
        } else if (rank < alive) {
            // rank is a receiver */
            MPI_Status status;
            int src = rank - alive/2;
            MPI_Recv (&sum, 1, MPI_LONG_LONG, src, 0, MPI_COMM_WORLD, &status);
        }
    }

    // every rank has the correct sum and can now compute the mean
    double mean = 1.0*sum/N;

    // each rank computes a partial sum of difference squares
    double sum_diff_sq = 0;
    for (long long i=1+rank;i<=N;i+=size) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    // use parallel message passing to reduce the partial sums of difference squares 
    // with result on rank 0 (we assume that size = 2^k for some integer k >= 0)
    alive = size;
    while (alive > 1) {
        if (rank < alive/2) {
            // rank is a receiver
            double rank_sum_diff_sq;
            MPI_Status status;
            int src = rank + alive/2;
            MPI_Recv (&rank_sum_diff_sq, 1, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, &status);
            sum_diff_sq += rank_sum_diff_sq;
        } else if (rank < alive) {
            // rank is a sender */
            int dest = rank - alive/2;
            MPI_Send (&sum_diff_sq, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
        }
        alive = alive/2;
    }

    // only rank 0 has the correct sum of diff sqs
    // and can now compute the correct variance
    // (other ranks will not compute the correct variance)
    double variance = sum_diff_sq/N;

    // calculate the standard deviation
    double stdev = sqrt(variance);

    // stop the timer
    end_time = MPI_Wtime();

    // only rank 0 has the correct standard deviation
    if (rank == 0) {
        printf ("num ranks = %d, elapsed time = %g\n",size,end_time-start_time);
        printf ("standard deviation is %.3lf, sqrt((N^2-1)/12) is %.3lf\n",
                stdev,sqrt((N*N-1)/12.0));
    }

    MPI_Finalize();
}

